# molflow-updater

Information for the built-in auto-updater in Molflow and Synrad.

Google analytics is provided separately, the updater sends events to Google Analytics tracking ID `UA-86802533-2`

The updater should check the following links:

- <https://gitlab.cern.ch/molflow_synrad/molflow-updater/-/raw/master/autoupdate_molflow.xml>
- <https://gitlab.cern.ch/molflow_synrad/molflow-updater/-/raw/master/autoupdate_synrad.xml>