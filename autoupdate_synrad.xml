<?xml version="1.0"?>
<UpdateFeed>
    <Branches>
        <Branch name="synrad_public" description="SynRad+ stable releases, for all operating systems since 1.5">
            <UpdateManifest>
                <Version id="1508" name="1.5.8_beta" date="2025-02-03" enabled="true"/>
                <ChangeLog>
                    <Global>
<![CDATA[
# Interface
- Reset simulation on reflection model change
- Define facet Move command using beam trajectory points or selected facet's properties
- Don't close Global Settings on setting change

# Bugfix
- Remove non-existent plots from Profile Plotter and Spectrum Plotter
- Reflection model setting refreshed on file load
- Fix crash on copy mirror command
- Particle Logger: correctly log absorbed photons in low flux mode
- Recompile for update GNU13 and macOS compatible libraries
]]>
                    </Global>
                </ChangeLog>
                <ValidForOS>
                    <OS id="win" name="Windows" prefix="synrad_win" />
                    <OS id="debian" name="Linux (Debian)" prefix="synrad_debian" />
                    <OS id="fedora" name="Linux (Fedora)" prefix="synrad_fedora" />
                    <OS id="mac_intel" name="MacOS (Intel)" prefix="synrad_mac" />
                    <OS id="mac_arm" name="MacOS (ARM)" prefix="synrad_mac_arm" />
                </ValidForOS>
                <Content zipUrlPathPrefix="https://gitlab.cern.ch/api/v4/projects/5580/packages/generic/Synrad/1.5.8_beta/"/>
                <FilesToCopy>
                    <Global>
                        <File name="synrad.cfg" />
                        <File name="updater_config.xml" />
                    </Global>
                </FilesToCopy>
            </UpdateManifest>
            <UpdateManifest>
                <Version id="1507" name="1.5.7_beta" date="2023-12-08" enabled="true"/>
                <ChangeLog>
                    <Global>
<![CDATA[
# Feature
- Allow particle logger to record only absorbed hits

# Interface
- Support for progressbars with wide/multiline status
- Added few missing options to Global Settings

# Bugfix
- Fixed a few OpenGL crashes related to camera views
- Fixed convergence plotter interface elements becoming black rectangles
- Fixed crash at "auto-create difference of 2 facets" function
- Fixed crash at 0-depth geometries at certain camera angles
- MPI version compiles again (although Synrad not tested with MPI yet)
- Better flux/power estimation for particle logger export
]]>
                    </Global>
                </ChangeLog>
                <ValidForOS>
                    <OS id="win" name="Windows" prefix="synrad_win" />
                    <OS id="debian" name="Linux (Debian)" prefix="synrad_debian" />
                    <OS id="fedora" name="Linux (Fedora)" prefix="synrad_fedora" />
                    <OS id="mac_intel" name="MacOS (Intel)" prefix="synrad_mac" />
                    <OS id="mac_arm" name="MacOS (ARM)" prefix="synrad_mac_arm" />
                </ValidForOS>
                <Content zipUrlPathPrefix="https://gitlab.cern.ch/api/v4/projects/5580/packages/generic/Synrad/1.5.7_beta/"/>
                <FilesToCopy>
                    <Global>
                        <File name="synrad.cfg" />
                        <File name="updater_config.xml" />
                    </Global>
                </FilesToCopy>
            </UpdateManifest>
            <UpdateManifest>
                <Version id="1506" name="1.5.6_beta" date="2023-02-17" enabled="true"/>
                <ChangeLog>
                    <Global>
<![CDATA[
# Bugfix
- Fixed saving textures with wrong facet index
- Fixed CLI compression of syn7z files
- Fixed Undo command for Split, Build intersection and Align
- Fixed spectrum saving
# Interface
- Better log language
- Enable console window for Windows
]]>
                    </Global>
                </ChangeLog>
                <ValidForOS>
                    <OS id="win" name="Windows" prefix="synrad_win" />
                    <OS id="debian" name="Linux (Debian)" prefix="synrad_debian" />
                    <OS id="fedora" name="Linux (Fedora)" prefix="synrad_fedora" />
                    <OS id="mac_intel" name="MacOS (Intel)" prefix="synrad_mac" />
                    <OS id="mac_arm" name="MacOS (ARM)" prefix="synrad_mac_arm" />
                </ValidForOS>
                <Content zipUrlPathPrefix="https://gitlab.cern.ch/api/v4/projects/5580/packages/generic/Synrad/1.5.6_beta/"/>
                <FilesToCopy>
                    <Global>
                        <File name="synrad.cfg" />
                        <File name="updater_config.xml" />
                    </Global>
                </FilesToCopy>
            </UpdateManifest>
            <UpdateManifest>
                <Version id="1505" name="1.5.5_beta" date="17-01-2023" enabled="true"/>
                <ChangeLog>
                    <Global>
<![CDATA[
# Bugfix
- File->Insert menu now works.
- Fixed bug that saved convergence values once per each facet.
]]>
                    </Global>
                </ChangeLog>
                <ValidForOS>
                    <OS id="win" name="Windows" prefix="synrad_win" />
                    <OS id="mac_intel" name="MacOS (Intel)" prefix="synrad_mac" />
                    <OS id="mac_arm" name="MacOS (ARM)" prefix="synrad_mac_arm" />
                    <OS id="debian" name="Linux (Debian)" prefix="synrad_debian" />
                    <OS id="fedora" name="Linux (Fedora)" prefix="synrad_fedora" />
                </ValidForOS>
                <Content zipUrlPathPrefix="https://gitlab.cern.ch/api/v4/projects/5580/packages/generic/Synrad/1.5.5_beta/"/>
                <FilesToCopy>
                    <Global>
                        <File name="synrad.cfg" />
                        <File name="updater_config.xml" />
                    </Global>
                </FilesToCopy>
            </UpdateManifest>
            <UpdateManifest>
                <Version id="1504" name="1.5.4_beta" date="13-01-2023" enabled="true"/>
                <ChangeLog>
                    <Global>
<![CDATA[
# Feature
- Add S global coordinate to regions
- Allow to refer BXY file to global S beam coordinate
- Allow global and local magnetic field component definition (see docs)
# Bugfix
- Normalize starting direction vector to 1 if not done by user
]]>
                    </Global>
                </ChangeLog>
                <ValidForOS>
                    <OS id="win" name="Windows" prefix="synrad_win" />
                    <OS id="mac_intel" name="MacOS (Intel)" prefix="synrad_mac" />
                    <OS id="mac_arm" name="MacOS (ARM)" prefix="synrad_mac_arm" />
                    <OS id="debian" name="Linux (Debian)" prefix="synrad_debian" />
                    <OS id="fedora" name="Linux (Fedora)" prefix="synrad_fedora" />
                </ValidForOS>
                <Content zipUrlPathPrefix="https://gitlab.cern.ch/api/v4/projects/5580/packages/generic/Synrad/1.5.4_beta/"/>
                <FilesToCopy>
                    <Global>
                        <File name="synrad.cfg" />
                        <File name="updater_config.xml" />
                    </Global>
                </FilesToCopy>
            </UpdateManifest>
            <UpdateManifest>
                <Version id="1503" name="1.5.3_beta" date="16-12-2022" enabled="true"/>
                <ChangeLog>
                    <Global>
<![CDATA[
Interface:
- New Global Settings layout unified with Molflow
- Non-planar facet higlighting can be toggled
Bugfix:
- Not retexturing all facets for geometry operations (slow)
- Fix crash when creating difference of holed facets
]]>
                    </Global>
                </ChangeLog>
                <ValidForOS>
                    <OS id="win" name="Windows" prefix="synrad_win" />
                    <OS id="mac_intel" name="MacOS (Intel)" prefix="synrad_mac" />
                    <OS id="mac_arm" name="MacOS (ARM)" prefix="synrad_mac_arm" />
                    <OS id="debian" name="Linux (Debian)" prefix="synrad_debian" />
                    <OS id="fedora" name="Linux (Fedora)" prefix="synrad_fedora" />
                </ValidForOS>
                <Content zipUrlPathPrefix="https://gitlab.cern.ch/api/v4/projects/5580/packages/generic/Synrad/1.5.3_beta/"/>
                <FilesToCopy>
                    <Global>
                        <File name="synrad.cfg" />
                        <File name="updater_config.xml" />
                    </Global>
                </FilesToCopy>
            </UpdateManifest>
            <UpdateManifest>
                <Version id="1502" name="1.5.2_beta" date="09-12-2022" enabled="true"/>
                <ChangeLog>
                    <Global>
<![CDATA[
Bugfix:
- Fixed crash when only regions are in the geometry
- Removed region view selector window as caused crashes
- Fixed OpenGL crash when geometry has 0-width in one dimension
- Fixed memory leak when collapsing facets
- Fixed 100% CPU usage when simulation paused
- Fixed app updater checking for debian versions on fedora
]]>
                    </Global>
                </ChangeLog>
                <ValidForOS>
                    <OS id="win" name="Windows" prefix="synrad_win" />
                    <OS id="mac_intel" name="MacOS (Intel)" prefix="synrad_mac" />
                    <OS id="mac_arm" name="MacOS (ARM)" prefix="synrad_mac_arm" />
                    <OS id="debian" name="Linux (Debian)" prefix="synrad_debian" />
                    <OS id="fedora" name="Linux (Fedora)" prefix="synrad_fedora" />
                </ValidForOS>
                <Content zipUrlPathPrefix="https://gitlab.cern.ch/api/v4/projects/5580/packages/generic/Synrad/1.5.2_beta/"/>
                <FilesToCopy>
                    <Global>
                        <File name="synrad.cfg" />
                        <File name="updater_config.xml" />
                    </Global>
                </FilesToCopy>
            </UpdateManifest>
			<UpdateManifest>
                <Version id="1427" name="1.4.27" date="02-09-2020" />
                <ChangeLog>
                    <![CDATA[
Feature
- Added support for macOS and Linux
- Triangulate geometry on selected facets
- Regions: Curved quadrupole support

Interface
- 3D Viewer: Better highlighting for selected facets
- Region Editor/Info: Filenames with long paths scrolled to end by default
- FacetID view option (facet number in center)
- Files with long paths abbreviated in Recent menus

Fix
- Fixed "Ideal beam" not checked on loading
- Fixed freeze when loading memorized views
- Fixed missing lighting when loading memorized views
- Fix "Apply texture" button setting incorrect flags
- Fixed texture record flags loading in incorrect state
- Fixed textures not recording on reflection
- When inserting geometries, teleport numbers are correctly offset

Note: the updater config file will be rewritten for cross-platform compatibility.
You will see a "Check for updates?" dialog again, only one time.
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_win/synrad_win_1.4.27.zip" zipName="synrad_win_1.4.27.zip" folderName="synrad_win_1.4.27" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                </FilesToCopy>
            </UpdateManifest>
            <UpdateManifest>
                <Version id="1426" name="1.4.26" date="29-01-2020" />
                <ChangeLog>
                    <![CDATA[
Feature
- Combined function magnets (dipole and quadrupole)
Bugfixes
- Fixed incorrect display and collision finding with concave facets
- Added command to flip normal of old geometries
- Fixed bug when trying to load non-existing ZIP file
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_1.4.26.zip" zipName="synrad_1.4.26.zip" folderName="synrad_1.4.26" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
            </UpdateManifest>
            <UpdateManifest>
                <Version id="1425" name="1.4.25" date="05-04-2019" />
                <ChangeLog>
                    <![CDATA[
Bugfix:
- Fixed leaks after reflections on rough surfaces with low-flux mode
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_1.4.25.zip" zipName="synrad_1.4.25.zip" folderName="synrad_1.4.25" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
            </UpdateManifest>
            <UpdateManifest>
                <Version id="1424" name="1.4.24" date="21-12-2018" />
                <ChangeLog>
                    <![CDATA[
Feature:
- Support for facets belonging to all structures at once
Bugfix:
- Support for compressing files with hundreds of regions
- Apply button becomes active on Spectrum recording toggle
- Critical energy calculation more precise
- Fixed crash when pressing Cancel at Load region to  dialog
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_1.4.24.zip" zipName="synrad_1.4.24.zip" folderName="synrad_1.4.24" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
            </UpdateManifest>
            <UpdateManifest>
                <Version id="1423" name="1.4.23" date="24-05-2018" />
                <ChangeLog>
                    <![CDATA[
- Fixed a memory management error causing crash on geometry change
- Added About/License info
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_1.4.23.zip" zipName="synrad_1.4.23.zip" folderName="synrad_1.4.23" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
            </UpdateManifest>
            <UpdateManifest>
                <Version id="1422" name="1.4.22" date="16-05-2018" />
                <ChangeLog>
                    <![CDATA[
Bugfix
- Fixed hard visibility (with Texture view on) of selected facets in 2.6.67
- Fixed a crash when trying to delete non-existent structure
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_1.4.22.zip" zipName="synrad_1.4.22.zip" folderName="synrad_1.4.22" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
            </UpdateManifest>
            <UpdateManifest>
                <Version id="1421" name="1.4.21" date="14-05-2018" />
                <ChangeLog>
                    <![CDATA[
Feature
- All profiles and spectra count incident and absorbed flux/power/hits separately
- Added Particle Logger tool (Tools menu)
- Screenshot tool (CTRL+R)
- Starting structure changeable for each region
- SYN file advanced to version 10, earlier versions won't open files written with this version
Interface
- Support for right-handed coordinate system (change in Global Settings)
- Region editor: can copy beam settings from an other region
Bugfix
- Collapse bug fix
- Removed rough surface reflection angle limit (discrete lines in the reflection pattern)
- Checking for filename conflicts before saving to a syn7z file
- Fixed enabled/disabled controls in region editor
- Fixed "invalid autocorrelation length" message
- BXY file empty lines are skipped (previously gave error)
- Fixed inserting SYN file
- param file handle released if loading fails
- App updater can handle file write errors (crashed before)
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_1.4.21.zip" zipName="synrad_1.4.21.zip" folderName="synrad_1.4.21" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
            </UpdateManifest>
            <UpdateManifest>
                <Version id="1420" name="1.4.20" date="30-11-2017" />
                <ChangeLog>
                    <![CDATA[
Feature
- For integrity, spectrum and profiles are only recorded for the absorbed part of flux
Bugfix
- Collapse function works again
- Spectrums, profiles work again
- Right-click menus work again
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_1.4.20.zip" zipName="synrad_1.4.20.zip" folderName="synrad_1.4.20" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
            </UpdateManifest>
            <UpdateManifest>
                <Version id="1419" name="1.4.19" date="29-11-2017" />
                <ChangeLog>
                    <![CDATA[
Bugfix
- Wrong relaunch angle at teleport
- Fixed not saving "shade lines" and "show teleports" viewer settings
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_1.4.19.zip" zipName="synrad_1.4.19.zip" folderName="synrad_1.4.19" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
            </UpdateManifest>
        </Branch>
		<Branch name="synrad_public_win" description="SynRad+ stable releases for Windows 64-bit">
        <UpdateManifest>
                <Version id="1430" name="1.4.30" date="17-02-2021" />
                <ChangeLog>
                    <![CDATA[
Feature
- Support for non-square textures
Bugfix
- STL files load fix
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_win/synrad_win_1.4.30.zip" zipName="synrad_win_1.4.30.zip" folderName="synrad_win_1.4.30" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
		</UpdateManifest>
        <UpdateManifest>
                <Version id="1429" name="1.4.29" date="04-01-2021" />
                <ChangeLog>
                    <![CDATA[
Feature
- Added Convergence plotter
- Support for multiple bodies in one STL file
Interface
- Fix for mesh mixed state
- Allow dragging with D key and left mouse button hold
- Allow zooming with Z key and left mouse button hold
- "Select plotted" button in profile plotter
Bugfix
- Closing collapse/smart select windows aborts the process
- More verbose error message in case of Subprocess error
- Crash when deleting a structure which is selected
- Restore shift+scroll for Mac
- Fixed timeout when loading large files
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_win/synrad_win_1.4.29.zip" zipName="synrad_win_1.4.29.zip" folderName="synrad_win_1.4.29" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
		</UpdateManifest>
		<UpdateManifest>
                <Version id="1428" name="1.4.28" date="21-09-2020" />
                <ChangeLog>
                    <![CDATA[
Fix
- Fixed longer loading times under Windows
- Fixed a false notification of a subprocess timeout
- Fixed a leaks happening when simulation rough surface reflection
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_win/synrad_win_1.4.28.zip" zipName="synrad_win_1.4.28.zip" folderName="synrad_win_1.4.28" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
		</UpdateManifest>
		</Branch>
		<Branch name="synrad_public_mac" description="SynRad+ stable releases for macOS">
	        <UpdateManifest>
	                <Version id="1430" name="1.4.30" date="17-02-2021" />
	                <ChangeLog>
	                    <![CDATA[
Feature
- Support for non-square textures
Bugfix
- STL files load fix
]]>
	                </ChangeLog>
	                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_mac/synrad_mac_1.4.30.zip" zipName="synrad_mac_1.4.30.zip" folderName="synrad_mac_1.4.30" />
	                <FilesToCopy>
	                    <File name="synrad.cfg" />
	                    <File name="updater_config.xml" />
	                </FilesToCopy>
			</UpdateManifest>
			<UpdateManifest>
                <Version id="1429" name="1.4.29" date="04-01-2021" />
                <ChangeLog>
                    <![CDATA[
Feature
- Added Convergence plotter
- Support for multiple bodies in one STL file
Interface
- Fix for mesh mixed state
- Allow dragging with D key and left mouse button hold
- Allow zooming with Z key and left mouse button hold
- "Select plotted" button in profile plotter
Bugfix
- Closing collapse/smart select windows aborts the process
- More verbose error message in case of Subprocess error
- Crash when deleting a structure which is selected
- Restore shift+scroll for Mac
- Fixed timeout when loading large files
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_mac/synrad_mac_1.4.29.zip" zipName="synrad_mac_1.4.29.zip" folderName="synrad_mac_1.4.29" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
		</UpdateManifest>
	        <UpdateManifest>
	            <Version id="1430" name="1.4.30" date="17-02-2021" />
	            <ChangeLog>
	                <![CDATA[
Feature
- Support for non-squared textures
Bugfix
- STL files load fix
]]>
	            </ChangeLog>
	            <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_mac/synrad_mac_1.4.30.zip" zipName="synrad_mac_1.4.30.zip" folderName="synrad_mac_1.4.30" />
	            <FilesToCopy>
	                <File name="synrad.cfg" />
	                <File name="updater_config.xml" />
	            </FilesToCopy>
			</UpdateManifest>
			<UpdateManifest>
                <Version id="1429" name="1.4.29" date="04-01-2021" />
                <ChangeLog>
                    <![CDATA[
Feature
- Added Convergence plotter
- Support for multiple bodies in one STL file
Interface
- Fix for mesh mixed state
- Allow dragging with D key and left mouse button hold
- Allow zooming with Z key and left mouse button hold
- "Select plotted" button in profile plotter
Bugfix
- Closing collapse/smart select windows aborts the process
- More verbose error message in case of Subprocess error
- Crash when deleting a structure which is selected
- Restore shift+scroll for Mac
- Fixed timeout when loading large files
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_mac/synrad_mac_1.4.29.zip" zipName="synrad_mac_1.4.29.zip" folderName="synrad_mac_1.4.29" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
		</UpdateManifest>
		<UpdateManifest>
                <Version id="1428" name="1.4.28" date="21-09-2020" />
                <ChangeLog>
                    <![CDATA[
Fix
- Fixed longer loading times under Windows
- Fixed a false notification of a subprocess timeout
- Fixed a leaks happening when simulation rough surface reflection
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_mac/synrad_mac_1.4.28.zip" zipName="synrad_mac_1.4.28.zip" folderName="synrad_mac_1.4.28" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
			</UpdateManifest>
		</Branch>
		<Branch name="synrad_public_linux_fedora" description="SynRad+ stable releases for x64 Debian Linux">
	        <UpdateManifest>
                <Version id="1430" name="1.4.30" date="17-02-2021" />
                <ChangeLog>
                    <![CDATA[
Feature
- Support for non-square textures
Bugfix
- STL files load fix
]]>
                </ChangeLog>
                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_linux_fedora/synrad_linux_fedora_1.4.30.zip" zipName="synrad_linux_fedora_1.4.30.zip" folderName="synrad_linux_fedora_1.4.30" />
                <FilesToCopy>
                    <File name="synrad.cfg" />
                    <File name="updater_config.xml" />
                </FilesToCopy>
			</UpdateManifest>
	        <UpdateManifest>
                
			</UpdateManifest>
			<UpdateManifest>
	                <Version id="1428" name="1.4.28" date="21-09-2020" />
	                <ChangeLog>
	                    <![CDATA[
Fix
- Fixed longer loading times under Windows
- Fixed a false notification of a subprocess timeout
- Fixed a leaks happening when simulation rough surface reflection
]]>
	                </ChangeLog>
	                <Content zipUrl="https://molflow.web.cern.ch/sites/default/files/synrad_linux_fedora/synrad_linux_fedora_1.4.28.zip" zipName="synrad_linux_fedora_1.4.28.zip" folderName="synrad_linux_fedora_1.4.28" />
	                <FilesToCopy>
	                    <File name="synrad.cfg" />
	                    <File name="updater_config.xml" />
	                </FilesToCopy>
			</UpdateManifest>
		</Branch>
	</Branches>
</UpdateFeed>